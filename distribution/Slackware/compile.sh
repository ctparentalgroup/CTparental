if [ "`id -u`" -ne "0" ]; then
   if [ "$(groups | grep -cE "( sudo )|( sudo$)|(sudo)")" -eq "0" ]; then
      echo "You must bee root or in sudo group for executing this script"
      exit 0
   fi
fi
 
## desinstallation php7    
slackpkg remove php kdev-php
## installation php8.1
slackpkg install php81
   
function buildpackage() {
Namepkg="${1}"
cat="${2}"
Ver="${3}"
Ver="${Ver:="15.0"}"
echo "$Namepkg"
wget https://slackbuilds.org/slackbuilds/$Ver/$cat/$Namepkg.tar.gz
tar -xvzf $Namepkg.tar.gz
cd $Namepkg
url=$(grep "DOWNLOAD=" $Namepkg.info | awk -F'"' {'print  $2'})
wget $url
echo 'echo "installpkg $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE" > $CWD/intallcmd' >> $Namepkg.SlackBuild
sudo ./$Namepkg.SlackBuild
}

cd $HOME
Name="google-go-lang"
category="development"
slackverssion="15.0"
buildpackage $Name $category
cd $HOME
install=$(cat $Name/intallcmd )
$install
# force upgrade PATH for next build, withaout wait next login bash.
source /etc/profile.d/go.sh

Name="dnscrypt-proxy"
category="network"
slackverssion="15.0"
buildpackage $Name $category
cd $HOME
install=$(cat $Name/intallcmd )
$install

Name="lighttpd"
category="network"
slackverssion="15.0"
## création de l'utilisateur lighttpd
groupadd -g 208 lighttpd
useradd -u 208 -g lighttpd -d /var/www -s /bin/false -c "User for lighttpd" lighttpd
buildpackage $Name $category
cd $HOME
install=$(cat $Name/intallcmd )
$install

## création du répertoir de génération des sockets
## est atribution des droits
mkdir -p /var/lib/lighttps/sockets
chown -R lighttpd:lighttpd /var/lib/lighttps
server.bind = "127.0.0.1" /etc/lighttpd/lighttpd.conf

